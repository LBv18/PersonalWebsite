<?php

function getAll() {
    $url = "http://api.github.com/users/LBv17/repos?sort=updated";
    $opts = [
        'http' => [
            'method' => 'GET',
            'header' => [
                'User-Agent: PHP',
            ],
        ],
    ];

    $json = file_get_contents($url, false, stream_context_create($opts));
    $obj = json_decode($json);
    return $obj;

}

?>