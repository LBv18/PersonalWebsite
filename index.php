<?php

    if (isset($_POST["submit"])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $from = 'From: Contact Form, ' . $email;
        $to = 'lorenzobaldassarri16@gmail.com';
        $subject = $_POST['subject'];
        $errName = '';
        $errEmail = '';
        $errMessage = '';
        $errSubject = '';

        $body = "From: $name\n\n E-Mail: $email\n\n Message:\n $message";

        // Check if name has been entered
        if (!$_POST['name']) {
            $errName = 'Please enter your name';
        }

        // Check if email has been entered and is valid
        if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errEmail = 'Please enter a valid email address';
        }

        //Check if message has been entered
        if (!$_POST['message']) {
            $errMessage = 'Please enter your message';
        }

        //Check if message has been entered
        if (!$_POST['subject']) {
            $errSubject = 'Please enter your subject';
        }

        // If there are no errors, send the email
        if ($errName == '' && $errEmail == '' && $errMessage  == '' && $errSubject == '') {
            if (mail($to, $subject, $body, $from)) {
                $result = '<div class="alert alert-success">Thank You! I will be in touch</div>';
            } else {
                $result = '<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
            }
        }

    }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">

    <!--Icons-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>LB</title>
</head>

<body>

    <div class="container cover">
        <div class="row cover">
            <div class="col-md-6 order-first">
                
                <div id="name-container">
                    <h1 class="title">
                        <div class="name animate fadeInUp">Lorenzo</div>
                        <div class="name animate fadeInUp">Baldassarri</div>
                    </h1>
                </div>
    
                <div class="social-media">
                    <a href="https://github.com/LBv17">
                        <i class="fab fa-github icon"></i>
                    </a>
                    <a href="https://www.linkedin.com/in/lorenzo-baldassarri-65536316a/">
                        <i class="fab fa-linkedin icon"></i>
                    </a>  
                    <a href="https://www.instagram.com/yung_lb16/">
                        <i class="fab fa-instagram icon"></i>
                    </a>
                    <a href="https://twitter.com/LB16_">
                        <i class="fab fa-twitter icon"></i>
                    </a>
                    <a href="https://snapchat.com/add/lb1618">
                        <i class="fab fa-snapchat-ghost icon"></i>
                    </a>          
                </div>
    
            </div>
    
            <div class="col-md-6">
                <div class="container navigation-container">

                    <ul class="list-group list-group-flush">
                        <a href="#about" class="list-group-item list-group-item-action nav-text">
                            About Me
                            <i class="fas fa-user-astronaut nav-icon"></i>
                        </a>
            
                        <a href="#projects" class="list-group-item list-group-item-action nav-text">
                            Projects
                            <i class="fas fa-archive nav-icon"></i>
                        </a>

                        <a href="#contact" class="list-group-item list-group-item-action nav-text"> 
                                Contact
                            <i class="fas fa-at nav-icon"></i> 
                        </a>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="container about-container" id="about">
        <div class="row">
            <div class="col-md-12 heading-about">
                <h1>About Me</h1>
            </div>
        </div>
        <div class="col-md-12 text-justify">
            <p class="about-text">Hi, my name is Lorenzo, I love to design, code and create.
                I'm a 17 year old developer studying at Kantonschule and Berufschule Baden in Switzerland. 
                <br>
                I love to design, code and create.
                My goal is to develop innovative and well designed products.  
                I have advanced knowledge and almost 3 years of experience with Java & JavaFX, a good understanding of Swift andsome experience with C++. 
                I'm also experienced with web developement, on the front side with HTML, CSS, Bootstrap and on the server side with PHP and mySQL.
            </p>
        </div>
    </div>

    <div class="container project-container" id="projects">
        
        <div class="row">
            <div class="col-md-12 heading-projects">
                <h1>Projects</h1>

                <h3>You can find the source code to each project on my <a href="https://github.com/LBv17">github</a>.</h3>
            </div>                <br>
        </div>

        <div class="row">
            
            <?php
            require 'repositories.php';
            $test = getAll();
            //print_r($test);
            foreach ($test as $repo) {
                // print '<a href="' . $repo->html_url . '">' . $repo->name . '</a><br />';
                print '<div class="col-md-6">
                            <div class="card text-white bg-dark">
                                <div class="card-header">
                                    ' . $repo->name . '
                                    <span class="badge badge-success" style="float:right">' . $repo->language . '</span>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                            ' . $repo->description . '
                                    </p>
                                    <p>
                                        <a class="download-button" href="' . $repo->html_url . '" download="">Visit on Github</a>
                                    </p>
                                    <footer class="blockquote-footer">Updated by L.B. on Github at
                                        <cite title="Source Title">' . str_replace(['T', 'Z'], ' ', $repo->updated_at) . '</cite>
                                    </footer>
                                </div>
                            </div>
                        </div>';
            }


            ?>


        </div>
    </div>

    <div class="container contact-container" id="contact">
        <div class="row">
            <div class="col-md-12 heading-contact">
                <h1>Contact</h1>
                <br>
                <h3>Feel free to contact me on any of the listed social networks on the <a href="#">front page</a> or via the form below.</h3>
            </div>
            <div class="col-md-12">

                <form action="index.php#contact" role="form" method="post" style="margin-top: 50px;">
                <div class="form-group">

		            <div class="col-sm-12">
                            <?php if (!empty($result)) {echo $result;} ?>	
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="name">First & Last Name *</label>
                            <input type="text" class="form-control" name="name" id="" placeholder="First & Last Name" required="required" data-error="Firstname is required.">
                            <?php if (!empty($errName)) {echo "<p class='text-danger'>$errName</p>"; } ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email *</label>
                            <input type="email" class="form-control" name="email" id="" placeholder="Email" required="required" data-error="Valid email is required.">
                            <?php if (!empty($errEmail)) {echo "<p class='text-danger'>$errEmail</p>";}?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">Subject *</label>
                            <input type="text" class="form-control" name="subject" id="" placeholder="Subject" required="required" data-error="Valid email is required.">
                            <?php if (!empty($errSubject)) {echo "<p class='text-danger'>$errSubject</p>";}?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message *</label>
                                <textarea  class="form-control"  name="message" id="form_message" placeholder="..." rows="4" required="required"
                                    data-error="Please, enter a message."></textarea>
                                <?php if (!empty($errMessage)) { echo "<p class='text-danger'>$errMessage</p>"; }?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="submit" name="submit" class="btn btn-success btn-send" value="Send message">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <p class="text-muted">
                            <strong>*</strong> These fields are required.
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <footer class="pt-4 my-md-5 pt-md-5 border-top">
            <div class="container">

                <div class="row">
                    <div class="col-sm-6">
                        <h6 class="footer-lb" >Lorenzo Baldassarri 2018 ©</h6>
                    </div>
                    <div class="col-sm-6">
                        <a class="back-to-top" href="#"><h6 class="back-to-top-text">Back to top</h6></a>
                    </div>
                </div>

            </div>
        </footer>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>

</html>
